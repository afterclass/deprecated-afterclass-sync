'use strict';

var _ = require('lodash'),
    util = require('util'),
    mysql = require('mysql'),
    config = require('../config'),
    helpers = require('../helpers');

function Users(connection) {
    this.connection = connection;
}

Users.prototype.saveUsers = function(users) {
    for (var userId in users) {
        if (users.hasOwnProperty(userId)) {
            console.log('Saving user ['+userId+']');
            this.saveUser(users[userId], userId);
        }
    }
};

Users.prototype.saveUser = function(user, userId){

    var _this = this;

    //Update user structure
    user.user_id = userId;

    if(typeof user.picture !== 'undefined' &&
       typeof user.picture.data !== 'undefined' &&
        typeof user.picture.data.url !== 'undefined'
    ) {
        user.profile_picture = user.picture.data.url;
    }


    //Duplicate user
    var simpleUser = {};
    _.assign(simpleUser, user);

    //1. Delete user (with all data)
    _this.connection.query('DELETE FROM users WHERE user_id='+mysql.escape(user.user_id), function(err, rows) {

        if (err) {
            console.log('Error: Can\'t delete user ['+user.user_id+']');
            console.log(err);
        } else { //2. Save user
            console.log('Success: Deleted user ['+user.user_id+']');

            _this.connection.query('INSERT INTO users SET ?', helpers.formatObject(simpleUser, config.allowedFields.users), function(err, result) {
                if(err) {
                    console.log('Error: Can\'t add new user ['+user.user_id+']');
                    console.log(err);
                } else {
                    console.log('Success: Added new user ['+user.user_id+']');
                    //2.1 Save user institutes
                    if(typeof user.target_institutes !== 'undefined') {
                        _this.saveUserInstitutes(user.target_institutes, user.user_id);
                    }
                }
            });
        }

    });

};

Users.prototype._saveSubject = function(subjectName, degreeId, userId){
    var tempSubject = {
        name: subjectName,
        degree_id: degreeId,
        user_id: userId
    };
    this.connection.query('INSERT INTO user_subjects SET ?', tempSubject, function(err, result) {
        if(err) {
            console.log('Error: Can\'t save new subject [' + tempSubject.name + '] for degree [' + tempSubject.degree_id + ']');
            console.log(err);
        } else {
            console.log('Success: Added new subject ['+tempSubject.name+'] for degree [' + tempSubject.degree_id + ']');
        }
    });
};

Users.prototype._saveDegree = function(degreeName, instituteId, userId, subjects){
    var _this = this;
    return function(){ //Create a closure
        var tempDegree = {
            name: degreeName,
            institute_id: instituteId,
            user_id: userId
        };
        _this.connection.query('INSERT INTO user_degrees SET ?', tempDegree, function(err, result) {
            if(err) {
                console.log('Error: Can\'t save new degree [' + tempDegree.name + '] for institute [' + tempDegree.institute_id + ']');
                console.log(err);
            } else {
                console.log('Success: Added new degree [' + tempDegree.name + '] for institute ['+tempDegree.institute_id+']');

                //Subject
                for(var i = 0; i < subjects.length; i++) {
                    _this._saveSubject(subjects[i], result.insertId, userId);
                }
            }
        });
    };
};

Users.prototype.saveUserInstitutes = function(targetInstitutes, userId){

    console.log('Saving target institutes for user ['+userId+']');
    var _this = this;

    //Institutes
    for (var institute in targetInstitutes) {
        if (targetInstitutes.hasOwnProperty(institute)) {
            var tempInstitute = {
                name: institute,
                user_id: userId
            };
            _this.connection.query('INSERT INTO user_institutes SET ?', tempInstitute, function(err, result) {
                if(err) {
                    console.log('Error: Can\'t save new institute ['+institute+'] for user ['+userId+']');
                    console.log(err);
                } else {
                    console.log('Success: Added new institute ['+institute+'] for user ['+userId+']');

                    //Degrees
                    for (var degree in targetInstitutes[institute]) {
                        if (targetInstitutes[institute].hasOwnProperty(degree)) {
                            _this._saveDegree(degree, result.insertId, userId, targetInstitutes[institute][degree])();
                        }
                    }
                }
            });
        }
    }
};

module.exports = Users;