'use strict';

var _ = require('lodash'),
    Q= require('q');

module.exports.formatObject = function(obj, allowedField) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            if(_.indexOf(allowedField, prop) === -1) {
                delete obj[prop];
            }
        }
    }

    return obj;
};


module.exports.getLastUpdateDate = function(connection, tableName, callback){
    connection.query('SELECT update_date from '+tableName+' order by update_date DESC LIMIT 1', function(err, rows) {
        if (err) {
            console.log('Error: Can\'t retrieve update_date of table ['+tableName+']');
            console.log(err);
        } else {
            if(rows.length > 0) {
                callback(rows[0].update_date);
            } else {
                callback(0);
            }
        }
    });
};

module.exports.getSyncTrack = function(connection, syncName){

    var deferred = Q.defer();

    connection.query('SELECT * from sync_track where sync_name = \''+syncName+'\' LIMIT 1', function(err, rows) {
        if (err) {
            console.log('Error: Can\'t retrieve sync tracking of sync ['+syncName+']'.red);
            deferred.reject(err);
        } else if(rows.length === 0){
            insertSyncTrackSuccess(connection, syncName);
            console.log('Notice: Sync track for ['+syncName+'] was not found. New one added.'.yellow);
            deferred.reject(new Error('Info: Sync track for ['+syncName+'] was not found. New one added.'));
        } else {
            console.log('Success: Found ['.green +syncName+'] sync track record'.green);
            deferred.resolve(rows[0]);
        }
    });

    return deferred.promise;
};

function insertSyncTrackSuccess(connection, syncName){

    var deferred = Q.defer();

    connection.query('INSERT INTO sync_track (sync_name, last_successful_run) VALUES (\''+syncName+'\', UNIX_TIMESTAMP())', function(err, rows) {
        if (err) {
            console.log('Error: Can\'t add new sync_track record for ['.red + syncName.red +']'.red);
            deferred.reject(err);
        }  else {
            console.log('Success: New sync_track record was created for ['.green + syncName.green +']'.green);
            deferred.resolve();
        }
    });

    return deferred.promise;
}
module.exports.insertSyncTrackSuccess = insertSyncTrackSuccess;

module.exports.updateSyncTrackSuccess = function(connection, syncName){

    var deferred = Q.defer();

    connection.query('UPDATE sync_track set last_successful_run = UNIX_TIMESTAMP(), last_status = \'success\' where sync_name = \''+syncName+'\'', function(err, rows) {
        if (err) {
            console.log('Error: Can\'t set last_successful_run of sync ['.red +syncName+']'.red);
            deferred.reject(err);
        }  else {
            console.log('Success: Updated last_successful_run of sync ['.green +syncName+']'.green);
            deferred.resolve();
        }
    });

    return deferred.promise;
};

module.exports.updateSyncTrackError = function(connection, syncName, error) {
    var deferred = Q.defer();

    connection.query('UPDATE sync_track set last_status = \'error\', Last_error_message = \''+error+'\' where sync_name = \''+syncName+'\'', function(err, rows) {
        if (err) {
            console.log('Error: updateSyncTrackError - Can\'t report error ['.red +error+'] of ['.red +syncName+']'.red);
            deferred.reject(err);
        }  else {
            console.log('Success: updateSyncTrackError reported an error of sync ['.green +syncName.red+']'.green);
            deferred.resolve();
        }
    });

    return deferred.promise;
};

/*
module.exports.getLastUpdateDateFireBase = function(fireBaseInterface, collectionName){

    var deferred = Q.defer();

    fireBaseInterface.ref.child(collectionName).orderByChild('update_date').limitToLast(1).on('value', function(payment) {
        var paymentObject = payment.val();
        if(paymentObject) {
            deferred.resolve(paymentObject.update_date);
        } else {
            deferred.resolve(0);
        }
    }, function(err) {
        deferred.reject(err);
    });

    return deferred.promise;

};*/
