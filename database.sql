#SYNC TRACK
CREATE TABLE `sync_track` (
  `sync_name` varchar(50) NOT NULL,
  `last_successful_run` int(11) DEFAULT NULL,
  `last_status` varchar(50) DEFAULT NULL,
  `last_error_message` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`sync_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#PAYMENTS
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(250) NOT NULL DEFAULT '0',
  `payment_hash` varchar(50) NOT NULL DEFAULT '0',
  `amount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `receipt_id` varchar(50) NOT NULL DEFAULT '0',
  `update_date` int(10) unsigned NOT NULL DEFAULT '0',
  `create_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `payment_hash` (`payment_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
SELECT * FROM firebase.payments;

#POSTS
CREATE TABLE `posts` (
  `post_id` varchar(100) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `body` mediumtext,
  `status` varchar(100) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `create_date` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `img_id` mediumtext,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `potential_tutors` (
  `post_id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `status_update_date` int(11) DEFAULT NULL,
  `firebase_key` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`post_id`,`user_id`),
  CONSTRAINT `potential_tutors_post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `replies` (
  `reply_id` varchar(100) NOT NULL,
  `post_id` varchar(100) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `body` mediumtext,
  `create_date` timestamp NULL DEFAULT NULL,
  `img_id` mediumtext,
  PRIMARY KEY (`reply_id`),
  CONSTRAINT `replies_post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#USERS
CREATE TABLE `users` (
  `user_id` varchar(100) NOT NULL,
  `amazon_endpoint_arn` varchar(500) DEFAULT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `is_teacher` bit(1) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `institute` varchar(50) DEFAULT NULL,
  `degree` varchar(50) DEFAULT NULL,
  `create_date` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `profile_picture` mediumtext,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_institutes` (
  `institute_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) NOT NULL,
  PRIMARY KEY (`institute_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `user_degrees` (
  `degree_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `institute_id` int(11) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`degree_id`),
  CONSTRAINT `institute_id` FOREIGN KEY (`institute_id`) REFERENCES `user_institutes` (`institute_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;


CREATE TABLE `user_subjects` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`subject_id`),
  CONSTRAINT `degree_id` FOREIGN KEY (`degree_id`) REFERENCES `user_degrees` (`degree_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;