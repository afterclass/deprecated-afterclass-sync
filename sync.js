'use strict';

var config = require('./config'),
    mysql = require('mysql'),
    FirebaseInterface = require('./firebaseInterface'),
    Posts = require('./posts/posts.mysql'),
    Users = require('./users/users.mysql'),
    Payments = require('./payments/payments'),
    helpers = require('./helpers'),
    Q = require('q');


for (var syncId = 0; syncId < config.databases.length; ++syncId) {
    (function() {
        var constSyncId = syncId;
        var connection = mysql.createConnection(config.databases[constSyncId].mysql);
        connection.query('DELETE posts, users FROM posts, users',
            function(err) {

            var firebase = new FirebaseInterface(config.databases[constSyncId].firebase.url, config.databases[constSyncId].firebase.token);
            var payments = new Payments(connection, config.databases[constSyncId].firebase.url, config.databases[constSyncId].firebase.token);
            var mysqlPosts = new Posts(connection);
            var mysqlUsers = new Users(connection);
            console.log('start sync on connection ' + constSyncId);

            //Run syncs
            if (process.argv.length <= 2 || process.argv.indexOf('posts') > -1) {
                console.log('Running posts sync..');
                helpers.getLastUpdateDate(connection, 'posts', function (timestamp) {
                    firebase.getPostsFrom(timestamp, function (posts) {
                        if (posts) {
                            console.log(constSyncId + ') Processing [' + Object.keys(posts).length + '] posts..');
                            mysqlPosts.savePosts(posts);
                        }
                    });
                });
            }

            if (process.argv.length <= 2 || process.argv.indexOf('users') > -1) {
                console.log('Running users sync..');
                helpers.getLastUpdateDate(connection, 'users', function (timestamp) {
                    firebase.getUsersFrom(timestamp, function (users) {
                        if (users) {
                            console.log(constSyncId + ') Processing [' + Object.keys(users).length + '] users..');
                            mysqlUsers.saveUsers(users);
                        }
                    });
                });
            }

            // Two-way sync of payments
            if (process.argv.length <= 2 || process.argv.indexOf('payments') > -1) {
                console.log('Running payments sync..');
                payments.sync();
            }
        });
    })();
}

// Script never exits (probably due to mysql connections)
setTimeout(function () {
    console.log('Exit script after 15    seconds');
    process.exit();
}, 15 * 1000);