'use strict';

var Q = require('q');

function FirebaseInterface(url, token) {
    var firebase = require('firebase');
    this.url = url;
    this.ref = new firebase(this.url);
    this.ref.authWithCustomToken(token, function(err, authData) {
        if (err) {
            console.log('Error: Can\'t authenticate with Firebase', err);
        } else {
            console.log("Success: Authenticated with Firebase", authData);
        }
    });
}

FirebaseInterface.prototype.getPostsFrom = function(timestamp, callback) {
    this.ref.child('posts').orderByChild('update_date').startAt(timestamp).on('value', function(posts) {
        callback(posts.val());
    }, function(err) {
        callback(err);
    });
};

FirebaseInterface.prototype.getPayments = function(callback) {
    this.ref.child('payments').on('value', function(payments) {
        callback(payments.val());
    }, function(err) {
        callback(err);
    });
};



FirebaseInterface.prototype.getUsersFrom = function(timestamp, callback) {
    this.ref.child('users').orderByChild('update_date').startAt(timestamp).on('value', function(users) {
        callback(users.val());
    }, function(err) {
        callback(err);
    });
};

FirebaseInterface.prototype.savePayments = function(payments, callback) {

    var _this = this,
        promises = [];

    for(var payment in payments) {
        promises.push(_this.savePayment(payments[payment].user_id, payments[payment].payment_hash, payments[payment]));
    }

    Q.allSettled(promises).then(function(results){
        var success = true;
        results.forEach(function (result) {
            if (result.state !== "fulfilled") {
                success = false;
            }
        });

        console.log('Payments sync completed '+ (success?'without':'with') + ' errors');

        if(success) {
            callback(null);
        } else {
            callback(false);
        }

    });

};

FirebaseInterface.prototype.savePayment = function(userId, paymentId, payment) {

    var deferred = Q.defer();

    this.ref.child('payments/' + userId + '/' + paymentId).set(payment, function(err){
        if(err) {
            console.log('Error: Can\'t set payment ['+paymentId+'] for user ['+userId+'] with data ['+JSON.stringify(payment)+']');
            deferred.reject(err);
        } else {
            console.log('Success: Saved payment ['+paymentId+'] for user ['+userId+']');
            deferred.resolve(paymentId);
        }
    });

    return deferred.promise;

};

module.exports = FirebaseInterface;
