'use strict';

var _ = require('lodash'),
    util = require('util'),
    mysql = require('mysql'),
    config = require('../config'),
    helpers = require('../helpers');

function Posts(connection) {
    this.connection = connection;
}

Posts.prototype.savePosts = function(posts) {
    for (var postId in posts) {
        if (posts.hasOwnProperty(postId)) {
            console.log('Processing post ['+postId+']');
            this.savePost(posts[postId], postId);
        }
    }
};

Posts.prototype.savePost = function(post, postId){

    var _this = this;

    //Update post structure
    post.post_id = postId;
    post.user_id = post.user;

    //Duplicate post
    var simplePost = {};
    _.assign(simplePost, post);

    //Delete post
    _this.connection.query('DELETE FROM posts WHERE post_id='+mysql.escape(post.post_id), function(err, rows) {

        if (err) {
            console.log('Error: Can\'t delete post ['+post.post_id+'] with error ['+err+']');
        } else { //2. Save post
            console.log('Success: Deleted post ['+post.post_id+']');

            //Save post
            _this.connection.query('INSERT INTO posts SET ?', helpers.formatObject(simplePost, config.allowedFields.posts), function(err, result) {
                if(err) {
                    console.log('Error: Can\'t add new post ['+post.post_id+'] with error ['+err+']');
                } else {
                    console.log('Success: Added new post ['+post.post_id+']');
                    //Save replies
                    _this.savePostReplies(post.replies, post.post_id);
                    //Save potential tutors
                    _this.savePostTutors(post.potential_tutors, post.post_id);
                }
            });
        }
    });

};

Posts.prototype.savePostReplies = function(replies, postId) {
    for (var replyId in replies) {
        if (replies.hasOwnProperty(replyId)) {
            console.log('Processing  reply ['+replyId+'] of post ['+postId+']');
            this.savePostReply(replies[replyId], replyId, postId);
        }
    }
};

Posts.prototype.savePostReply = function(reply, replyId, postId) {

    //Update reply structure
    reply.post_id = postId;
    reply.reply_id = replyId;
    reply.user_id = reply.user;

    //Save reply
    this.connection.query('INSERT INTO replies SET ?', helpers.formatObject(reply, config.allowedFields.replies), function(err, result) {
        if(err) {
            console.log('Error: Can\'t save reply ['+reply.reply_id+'] of post ['+reply.post_id+']');
            console.log(err);
        } else {
            console.log('Success: Reply ['+reply.reply_id+'] of post ['+reply.post_id+'] was saved');
        }
    });
};

Posts.prototype.savePostTutors = function(tutors, postId) {
    for (var tutor in tutors) {
        if (tutors.hasOwnProperty(tutor)) {
            console.log('Processing  potential tutor [' + tutor + '] of post [' + postId + ']');
            this.savePostTutor(tutors[tutor], tutor, postId);
        }
    }
};

Posts.prototype.savePostTutor = function(tutor, tutorId, postId) {

    //Update potential tutor structure
    tutor.post_id = postId;
    tutor.user_id = tutor.id!=null?tutor.id:tutorId;
    tutor.status = tutor.post_status;
    tutor.firebase_key = tutorId;

    //Save potential tutor
    this.connection.query('INSERT INTO potential_tutors SET ?', helpers.formatObject(tutor, config.allowedFields.potentialTutors), function(err, result) {
        if(err) {
            console.log('Error: Can\'t save potential tutor ['+tutor.firebase_key+'] of post ['+tutor.post_id+']');
            console.log(err);
        } else {
            console.log('Success: Potential tutor at index ['+tutor.firebase_key+'] of post ['+tutor.post_id+'] was saved');
        }
    });
};

module.exports = Posts;