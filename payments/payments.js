'use strict';

var config = require('../config'),
    PaymentsMySQL = require('./payments.mysql'),
    FirebaseInterface = require('../firebaseInterface'),
    helpers = require('../helpers'),
    Q = require('q'),
    async = require('async'),
    colors = require('colors');

function Payments(connection,firebaseUrl,firebaseToken) {
    this.connection = connection;
    this.paymentsMySQL = new PaymentsMySQL(connection);
    this.firebase = new FirebaseInterface(firebaseUrl, firebaseToken);
}

Payments.prototype.syncMySQLToFirebase = function(_this, callback) {

    console.log('Info: Starting MySQL -> Firebase sync'.green);

    helpers.getSyncTrack(_this.connection, 'payments').then(function(syncTrack){ // Get last running date so a partial sync may be done
        _this.paymentsMySQL.getPayments(syncTrack.last_successful_run).then(function(payments){ //Load MySQL payments
            console.log('Info: Processing ['.blue +payments.length+'] payments from MySQL'.blue);
            _this.firebase.savePayments(payments, callback); //Save payments
        });
    });

};

//Update 'current' payment statuses to 'pending' based on Firebase data
Payments.prototype.syncFirebaseStatusesToMySQL = function(_this, callback) {

    _this.firebase.getPayments(function(payments){
        if(payments) {
            console.log('Processing [' + Object.keys(payments).length + '] payments from Firebase..');
            _this.paymentsMySQL.updatePaymentsStatus(payments, callback); //Update status only current -> pending
        } else {
            console.log('Payments: no payments found in Firebase');
            callback(null);
        }
    });

};

Payments.prototype.sync = function() {

    var _this = this;

    async.waterfall([
        function(callback) { callback(null, _this) }, //Context 'this' is not available - that's a work around
        _this.syncFirebaseStatusesToMySQL,
        function(callback) { callback(null, _this) }, //Context 'this' is not available - that's a work around
        _this.syncMySQLToFirebase
    ], function(err, result){
        if(err) {
            console.log('Error: Payments sync failed ['+err+']');
            helpers.updateSyncTrackError(_this.connection, 'payments', err);
        } else {
            console.log('Success: payments sync completed ['+result+']');
            helpers.updateSyncTrackSuccess(_this.connection, 'payments');
        }
    });
};

module.exports = Payments;

