'use strict';

var _ = require('lodash'),
    util = require('util'),
    mysql = require('mysql'),
    config = require('../config'),
    helpers = require('../helpers'),
    Q = require('q'),
    moment = require('moment');

function PaymentsMySQL(connection) {
    this.connection = connection;
}

PaymentsMySQL.prototype.getPayments = function(timestamp) {

    var deferred = Q.defer();

    this.connection.query('SELECT * FROM payments where update_date >= '+timestamp, function(err, rows) {
        if(err) {
            console.log('Error: Can\'t get payments ['+JSON.stringify(err)+']');
            deferred.reject(err);
        } else {
            console.log('Success: retrieved payments');
            deferred.resolve(rows);
        }
    });

    return deferred.promise;

};

PaymentsMySQL.prototype.updatePaymentsStatus = function(payments, callback) {

    var _this = this,
        promises = [];

    for(var userId in payments) {
        if(payments.hasOwnProperty(userId)) {
            for(var paymentId in payments[userId]) {
                if(payments[userId].hasOwnProperty(paymentId)) {
                    //Update only 'pending' payments
                    if(payments[userId][paymentId].status === 'pending') {
                        console.log('Info: Updating a payment ['+paymentId+'] to pending status');
                        promises.push(_this.updatePayment(userId, payments[userId][paymentId].payment_hash, {
                            status: 'pending',
                            update_date: moment().utc().unix()
                        }));
                    }
                }
            }
        }
    }

    Q.allSettled(promises).then(function(results){
        var success = true;
        results.forEach(function (result) {
            if (result.state !== "fulfilled") {
                success = false;
            }
        });

        console.log('Payment status update completed '+ (success?'without':'with') + ' errors');

        if(success) {
            callback(null);
        } else {
            callback(false);
        }

    });

};

PaymentsMySQL.prototype.updatePayment = function(userId, paymentHash, payment) {

    var deferred = Q.defer();

    this.connection.query('UPDATE payments SET ? WHERE user_id = ' + this.connection.escape(userId) + ' AND payment_hash = ' + this.connection.escape(paymentHash), payment, function(err, rows) {
        if(err) {
            console.log('Error: Can\'t update payments record for payment ['+paymentHash+'] with error ['+JSON.stringify(err)+']');
            deferred.reject(err);
        } else {
            console.log('Success: update payment ['+paymentHash+'] with data ['+JSON.stringify(payment)+']');
            deferred.resolve(rows);
        }
    });

    return deferred.promise;
};

module.exports = PaymentsMySQL;